# Summary #

Muse is a header only compile and run-time reflection library built on top of C++17 features. It is designed to blend into the background of the language and hopefully provide transparent reflection abilities. Essentially the goal is for it to act like an extension of C++ with reflection capabilities.

It was developed mostly for learning purposes and meta-programming experimentation, some implementation details might be less-than-favorable. Nonetheless is functional and potentially useful.

*Note: Syntax is still in progress, some things aren't fully decided on (and thus there are two ways to access compile time data).*
*Future: it's possible a tool/build step could be used to generate the muse type, effectively eliminating most of the user setup.*

# License #

See the LICENSE file for more information. If you decide to use it feel free to send me an email, as I'd love to hear about it.

# Usage #

## Type Declaration ##

To use Muse reflection for a class, we need to declare the class as reflected. This is done by declaring a meta object using a custom language syntax.

Example:

```
#!c++

class test_class : public muse::runtime::object<test_class> // must derive from the run-time object for run-time access (optional for compile time reflection)
{
public:
    int m_member;
    int method() { return m_member; }
};

// declare that we're using meta information for test_class
using metatype(test_class)
{
    // declare the metaobject
    metaobject = decltype
    (
        metaclass() // declare as metaclass, optionally includes attributes and such
        [
            property(m_member), // declare a property
            method(method) // declare a method
        ]
    );
}

```

## Accessing a Muse Type ##

Accessing a meta type can be done by using typeof(). typeof() is just syntactic sugar to make it clear to readers that a muse type is being used, rather than a regularly-defined type.

Example:

```
#!c++

// Compile time:

using my_type = typeof(test_class);

// Run-time:

auto my_type = test_class::runtime_type();

```

## Accessing Methods and Properties ##

Method and Properties can be access via two means: muse::get_property/get_method, and from the type itself.

Example:

```
#!c++

// Compile time:

using my_member = muse::get_property<my_type, member(m_member)>; // member() will fetch a member tag. Can also use tag("m_member") if desired.
using my_method = muse::get_method<my_type, member(method)>;
// or
using my_member = my_type::properties::get<member(m_member))>;
using my_method = my_type::methods::get<member(method))>;

// using them

test_class inst;
std::cout << my_member::get_value(inst) << std::endl;
std::cout << my_method::call(inst) << std::endl;

// Run-time:

auto my_member = my_type->get_property("m_member");
auto my_method = my_type->get_method("method");

// using them

test_class inst;
std::cout << my_member->get_value(inst) << std::endl;
std::cout << my_method->call(inst) << std::endl;

```

## Attributes ##

Attributes can be added in the header of the type declaration.

Example:

```
#!c++
...
using metatype(test_class)
{
    metaobject = decltype
    (
        metaclass()
        (
            alias("Test Class") // give this class an alias attribute
        )
        [
            property(m_member)
            (
                alias("Test Member") // give this property an alias attribute
            ),
            method(method)
            (
                alias("Test Method") // give this method an alias attribute
            )
        ]
    );
}

// fetching the alias:

std::cout << muse::get_alias(my_type()) << std::endl;
std::cout << muse::get_alias(my_property()) << std::endl;
std::cout << muse::get_alias(my_method()) << std::endl;

```

## Future Ideas ##

Don't use tags for identification, rather simply use the member pointer value which can significantly decrease the template name sizes and potentially compile time.
Built-in attribute for methods that should serialize (run on save, run on load).
Support for STL containers and types and an interface for property expansion (for custom projects with custom types).

## Known Issues and Limitations ##

Currently only basic POD types are supported.
There are currently no developed attributes other than alias.
Base class/inheritance isn't fully supported yet.
The compiler errors generated from a failed build are atrocious (sorry, but that's what we get for playing so dirty!). Sadly not much that can be done here, I think.
Currently, member and tag use a fixed compile-time string type to identify types. These tags are unfortunately limited to 32 characters. Structs can be used in place of the tags, but must be declared up-front elsewhere.