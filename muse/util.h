#pragma once

#include <type_traits>

namespace muse
{
	namespace util
	{
		template<typename T>
		struct member_pointer_traits;

		template<typename Class, typename Ret, typename... Args>
		struct member_pointer_traits<Ret(Class::*)(Args...)>
		{
			using type = Ret(Class::*)(Args...);
			using class_type = Class;
			using return_type = Ret;
		};

		template<typename Class, typename Ret>
		struct member_pointer_traits<Ret Class::*>
		{
			using type = Ret Class::*;
			using class_type = Class;
			using return_type = Ret;
		};

		template< template<typename...> class, typename, typename = void >
		struct is_specialized : std::false_type {};

		template<template<typename...> class Template, typename T>
		struct is_specialized<Template, T, std::void_t<decltype(Template<T>{})>> : std::true_type {};

		template <typename Class, typename Meta, typename Enable = void>
		struct meta_binding;
	}
}