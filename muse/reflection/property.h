#pragma once

#include "method.h"
#include "tag.h"
#include "fieldlist.h"

#include <fstream>

namespace muse
{
	namespace detail
	{
		template <typename T, T, typename, typename = utility::type_list<>>
		struct property;

		template <typename Class, typename Type, Type(Class::* Member), typename Tag, typename... Attributes>
		struct property<Type(Class::*), Member, Tag, utility::type_list<Attributes...>> : Attributes...
		{
			using class_type = Class;
			using value_type = Type;
			using tag_type = Tag;
			static void set(Class& inst, const Type& value)                                     { inst.*Member = value; }
			static auto get(const Class& inst)                                                  { return inst.*Member; }
			static void set_value(Class& inst, const Type& value)                               { set(inst, value); }
			static auto get_value(const Class& inst)                                            { return get(inst); }
			static const char* name()                                                           { return Tag::c_str(); }
			static const std::size_t size()                                                     { return sizeof(Type); }
			template <typename Attribute> auto operator()(const Attribute& attrib)              { return property<Type(Class::*), Member, Tag, utility::type_list<Attribute>>(); }
			template <typename... Ts> auto operator()(const detail::field_list<Ts...>& attribs) { return property<Type(Class::*), Member, Tag, utility::type_list<Ts...>>(); }
		};

		template <typename Class, typename Type, const Type(Class::* Member), typename Tag, typename... Attributes>
		struct property<const Type(Class::*), Member, Tag, utility::type_list<Attributes...>> : Attributes...
		{
			using value_type = Type;
			using class_type = Class;
			using tag_type = Tag;
			static void set(Class& inst, const Type& value)                                     {}
			static Type get(const Class& inst)                                                  { return inst.*Member; }
			static void set_value(Class& inst, const Type& value)                               {}
			static Type get_value(const Class& inst)                                            { return get(inst); }
			static const char* name()                                                           { return Tag::c_str(); }
			static const std::size_t size()                                                     { return sizeof(Type); }
			template <typename Attribute> auto operator()(const Attribute& attrib)              { return property<const Type(Class::*), Member, Tag, utility::type_list<Attribute>>(); }
			template <typename... Ts> auto operator()(const detail::field_list<Ts...>& attribs) { return property<const Type(Class::*), Member, Tag, utility::type_list<Ts...>>(); }
		};

		template <typename Class, typename Type, Type*(Class::* Member), typename Tag, typename... Attributes>
		struct property<Type*(Class::*), Member, Tag, utility::type_list<Attributes...>> : Attributes...
		{
			using value_type = Type;
			using class_type = Class;
			using tag_type = Tag;
			static void set(Class& inst, const Type* value)                                     { inst.*Member = value; }
			static Type* get(const Class& inst)                                                 { return inst.*Member; }
			static void set_value(Class& inst, const Type& value)                               { *(inst.*Member) = value; }
			static Type get_value(const Class& inst)                                            { return *(inst.*Member); }
			static const char* name()                                                           { return Tag::c_str(); }
			static const std::size_t size()                                                     { return sizeof(Type); }
			template <typename Attribute> auto operator()(const Attribute& attrib)              { return property<Type(Class::*), Member, Tag, utility::type_list<Attribute>>(); }
			template <typename... Ts> auto operator()(const detail::field_list<Ts...>& attribs) { return property<Type(Class::*), Member, Tag, utility::type_list<Ts...>>(); }
		};

		template <typename Class, typename Type, const Type*(Class::* Member), typename Tag, typename... Attributes>
		struct property<const Type*(Class::*), Member, Tag, utility::type_list<Attributes...>> : Attributes...
		{
			using value_type = Type;
			using class_type = Class;
			using tag_type = Tag;
			static void set(Class& inst, const Type* value)                                     {}
			static Type* get(const Class& inst)                                                 { return inst.*Member; }
			static void set_value(Class& inst, const Type& value)                               {}
			static Type get_value(const Class& inst)                                            { return *(inst.*Member); }
			static const char* name()                                                           { return Tag::c_str(); }
			static const std::size_t size()                                                     { return sizeof(Type); }
			template <typename Attribute> auto operator()(const Attribute& attrib)              { return property<const Type(Class::*), Member, Tag, utility::type_list<Attribute>>(); }
			template <typename... Ts> auto operator()(const detail::field_list<Ts...>& attribs) { return property<const Type(Class::*), Member, Tag, utility::type_list<Ts...>>(); }
		};

		template <typename T0, T0 x0, typename Tag0, typename... Attribs0, template<typename T1, T1 x1, typename Tag1, typename... Attribs1> typename F, typename T1, T1 x1, typename Tag1, typename... Attribs1>
		auto operator,(const property<T0, x0, Tag0, utility::type_list<Attribs0...>>&, const F<T1, x1, Tag1, utility::type_list<Attribs1...>>&)
		{
			using Type0 = property<T0, x0, Tag0, utility::type_list<Attribs0...>>;
			using Type1 = F<T1, x1, Tag1, utility::type_list<Attribs1...>>;
			return detail::field_list<Tag0, Type0, Tag1, Type1>();
		}
	
		template<typename>
		struct is_property : std::false_type {};

		template<typename Type, Type x, typename Tag, typename Attributes>
		struct is_property<property<Type, x, Tag, Attributes>> : std::true_type {};
	}

	namespace util
	{
		template <typename Class, typename Field>
		struct meta_binding<Class, Field, typename std::enable_if<::muse::detail::is_property<Field>::value>::type>
		{
			meta_binding(Class& inst, Field& field) : inst(inst), field(field) {}
			auto get() { return field.get(inst); }
			void set(typename Field::value_type& value) { field.set(inst, value); }
			auto get_value() { return field.get_value(inst); }
			void set_value(typename Field::value_type& value) { field.set_value(inst, value); }
			Class& inst;
			Field& field;
		};
	}
}