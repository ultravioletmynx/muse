#pragma once

#ifdef _MSC_VER
#pragma warning(disable : 4503) // disable warning for Tag<> because the names can get too long
#endif

// unfortunate tag macro stuff
// maximum 32 characters, if they're not all used then the tag is padded with 0s
// simple and dirty but reliable
#define MACRO_GET_1(str, i) \
		(sizeof(str) > (i) ? str[(i)] : 0)

#define MACRO_GET_2(str, i) \
		MACRO_GET_1(str, i+0),  \
		MACRO_GET_1(str, i+1)  \

#define MACRO_GET_8(str, i) \
		MACRO_GET_2(str, i+0),   \
		MACRO_GET_2(str, i+2),   \
		MACRO_GET_2(str, i+4),   \
		MACRO_GET_2(str, i+6)

#define MACRO_GET_32(str, i) \
		MACRO_GET_8(str, i+0),   \
		MACRO_GET_8(str, i+8),   \
		MACRO_GET_8(str, i+16),   \
		MACRO_GET_8(str, i+24)

namespace muse
{
	namespace detail
	{
		template <char... letters>
		struct tag
		{
			static char const* c_str()
			{
				static constexpr char string[] = { letters..., '\0' };
				return string;
			}
		};
	}
}