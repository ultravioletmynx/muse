#pragma once

#include "../util.h"
#include "../serialization/streamserializer.h"

#include "property.h"
#include "method.h"
#include "tag.h"
#include "fieldlist.h"

namespace muse
{
	template <typename, typename = void, typename = void>
	struct type;

	template <typename Class, typename BaseClass, typename... Attributes>
	struct type<Class, BaseClass, utility::type_list<Attributes...>>
	{
		friend typename Class;

		template <typename T, T x, typename Tag, typename... Attribs>
		auto operator[](const detail::property<T, x, Tag, utility::type_list<Attribs...>>&)
		{
			return detail::type<Class, BaseClass, utility::type_list<Attributes...>, detail::field_list<Tag, detail::property<T, x, Tag, utility::type_list<Attribs...>>>>();
		}

		template <typename T, T x, typename Tag, typename... Attribs>
		auto operator[](const detail::method<T, x, Tag, utility::type_list<Attribs...>>&)
		{
			return detail::type<Class, BaseClass, utility::type_list<Attributes...>, detail::field_list<Tag, detail::method<T, x, Tag, utility::type_list<Attribs...>>>>();
		}

		template <typename... Ts>
		auto operator[](const detail::field_list<Ts...>&)
		{
			return detail::type<Class, BaseClass, utility::type_list<Attributes...>, detail::field_list<Ts...>>();
		}

		template <typename... Ts>
		auto operator()(Ts&&...)
		{
			return type<Class, BaseClass, utility::type_list<Ts...>>();
		}
	};

	template <typename Class>
	struct type<Class, void, void>
	{
		friend typename Class;

		template <typename T, T x, typename Tag, typename... Attribs>
		auto operator[](const detail::property<T, x, Tag, utility::type_list<Attribs...>>&)
		{
			return detail::type<Class, void, utility::type_list<Attributes...>, detail::field_list<Tag, detail::property<T, x, Tag, utility::type_list<Attribs...>>>>();
		}

		template <typename T, T x, typename Tag, typename... Attribs>
		auto operator[](const detail::method<T, x, Tag, utility::type_list<Attribs...>>&)
		{
			return detail::type<Class, void, utility::type_list<Attributes...>, detail::field_list<Tag, detail::method<T, x, Tag, utility::type_list<Attribs...>>>>();
		}

		template <typename... Ts>
		auto operator[](const detail::field_list<Ts...>&)
		{
			return detail::type<Class, void, utility::type_list<Attributes...>, detail::field_list<Ts...>>();
		}

		template <typename... Ts>
		auto operator()(Ts&&...)
		{
			return type<Class, void, utility::type_list<Ts...>>();
		}
	};

	namespace detail
	{
		template <typename T>
		struct auto_register
		{
			static bool registered;
		};

		template <typename T>
		bool auto_register<T>::registered = T::auto_register();

		template <typename, typename = void, typename = void, typename = void>
		class type;

		template <typename Class, typename BaseClass, typename... Attributes, typename... Fields>
		class type<Class, BaseClass, utility::type_list<Attributes...>, detail::field_list<Fields...>>
			: detail::auto_register<type<Class, BaseClass, utility::type_list<Attributes...>
			, detail::field_list<Fields...>>>, public Attributes...
		{
			using type_class = Class;
			using type_desc = type<Class, BaseClass, utility::type_list<Attributes...>, detail::field_list<Fields...>>;
			using fields = detail::field_list<Fields...>;

			template <typename T, typename F>
			friend void ::muse::for_each_field(F);

		public:
			type()
			{
				detail::auto_register<type_desc>::registered = true;
			}

			static bool auto_register()
			{
				runtime::type_registry::register_runtime_type<Class, type_desc>();
				return true;
			}

			template <typename Serializer = stream_serializer>
			static bool serialize_object(Class& object, const char* path, io::openmode mode = io::in)
			{
				return Serializer::serialize<type_desc, Class>(object, path, mode);
			}

			struct properties
			{
				template <typename Tag>
				using get = typename std::conditional<
					detail::is_property<typename fields::template get_tagged_element<Tag>::type>::value,
					typename fields::template get_tagged_element<Tag>::type, void
				>;
			};

			struct methods
			{
				template <typename Tag>
				using get = typename std::conditional<
					detail::is_method<typename fields::template get_tagged_element<Tag>::type>::value,
					typename fields::template get_tagged_element<Tag>::type, void
				>;
			};
		};

		template<typename>
		struct is_type : std::false_type {};

		template<typename Class, typename BaseClass, typename... Attributes, typename... Fields>
		struct is_type<type<Class, BaseClass, utility::type_list<Attributes...>, detail::field_list<Fields...>>> : std::true_type {};
	}

	namespace util
	{
		template <typename Class, typename Type>
		struct meta_binding<Class, Type, typename std::enable_if<::muse::detail::is_type<Type>::value>::type>
		{
			meta_binding(Class& inst, Type& field) : inst(inst), type(type) {}
			Class& inst;
			Type& type;
		};
	}
}