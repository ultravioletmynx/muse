#pragma once

#include <sstream>

namespace muse
{
	namespace utility
	{
		template<typename... Ts>
		struct type_list
		{
			template<typename T>
			using prepend = type_list<T, Ts...>;
		};

		template<std::size_t, typename...>
		struct element_type;

		template<typename T, typename... Ts>
		struct element_type<0, T, Ts...>
		{
			using type = T;
		};

		template<std::size_t I, typename T, typename... Ts>
		struct element_type<I, T, Ts...>
		{
			using type = typename element_type<I - 1, Ts...>::type;
		};

		template<std::size_t, std::size_t, typename...>
		struct element_type_range;

		template<std::size_t N, std::size_t I, typename T, typename... Ts>
		struct element_type_range<N, I, T, Ts...> : element_type_range<N, I - 1, Ts...>
		{
		};

		template<std::size_t N, typename T, typename... Ts>
		struct element_type_range<N, 0, T, Ts...>
		{
			using type = typename element_type_range<N, N - 1, Ts...>::type::template prepend<T>;
		};

		template<std::size_t I, std::size_t N>
		struct element_type_range<I, N>
		{
			using type = type_list<>;
		};

		template<class Ch, class Tr, std::size_t I, typename... Ts>
		struct type_printer
		{
			static void print(std::basic_ostream<Ch, Tr>& out, const type_list<Ts...>& t)
			{
				type_printer<Ch, Tr, I - 1, Ts...>::print(out, t);
				if (I < sizeof...(Ts))
				{
					out << ",";
				}
				out << typeid(typename element_type<I, Ts...>::type).name();
			}
		};
		template<class Ch, class Tr, typename... Ts>
		struct type_printer<Ch, Tr, 0, Ts...>
		{
			static void print(std::basic_ostream<Ch, Tr>& out, const type_list<Ts...>& t)
			{
				out << typeid(typename element_type<0, Ts...>::type).name();
			}
		};

		template<class Ch, class Tr, typename... Ts>
		struct type_printer<Ch, Tr, -1, Ts...>
		{
			static void print(std::basic_ostream<Ch, Tr>& out, const type_list<Ts...>& t) {}
		};

		template<class Ch, class Tr, typename... Ts>
		std::ostream& operator<<(std::basic_ostream<Ch, Tr>& out, const type_list<Ts...>& t)
		{
			type_printer<Ch, Tr, sizeof...(Ts) - 1, Ts...>::print(out, t);
			return out;
		}
	}
}