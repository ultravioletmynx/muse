#pragma once

#include "../util.h"
#include "tag.h"

namespace muse
{
	struct alias_attribute {};

	namespace detail
	{
		template <typename T>
		struct alias_attribute : ::muse::alias_attribute
		{
			static const char* alias_name() { return T::c_str(); }
		};
	}
}