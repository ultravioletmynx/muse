#pragma once

#include "typelist.h"

namespace muse
{
	namespace detail
	{
		template<typename T, typename... Ts>
		struct field_index {};

		template<typename T, typename... Ts>
		struct field_index<T, T, Ts...> : std::integral_constant<std::size_t, 0> {};

		template<typename T, typename U, typename... Ts>
		struct field_index<T, U, Ts...> : std::integral_constant<std::size_t, field_index<T, Ts...>::value + 1> {};

		template<typename S, typename T>
		struct field_type_list;

		template<typename... Ss, typename... Ts>
		struct field_type_list<utility::type_list<Ss...>, utility::type_list<Ts...>> : public std::tuple<Ts...>
		{
			template <typename T>
			using get_tagged_element = utility::element_type<field_index<T, Ss...>::value, Ts...>;

			template <std::size_t I>
			using get_element = utility::element_type<I, Ts...>;
		};

		template<typename... Ts>
		struct field_list : field_type_list<typename utility::element_type_range<2, 0, Ts...>::type, typename utility::element_type_range<2, 1, Ts...>::type>
		{
			template<typename... Ss>
			using append = field_list<Ts..., Ss...>;
		};

		template<std::size_t I = 0, typename TFunc, typename... Tuple>
		typename std::enable_if<I == (sizeof...(Tuple) / 2), bool>::type
			for_each_field(field_list<Tuple...>&, TFunc)
		{
			return false;
		}

		template<std::size_t I = 0, typename TFunc, typename... Tuple>
		typename std::enable_if<I < (sizeof...(Tuple) / 2), bool>::type
			for_each_field(field_list<Tuple...>& tup, TFunc func)
		{
			func(field_list<Tuple...>::get_element<I>::type());
			return for_each_field<I + 1, TFunc, Tuple...>(tup, func);
		}
		
		template <typename... T0, typename... T1>
		auto operator,(const field_list<T0...>&, const field_list<T1...>&)
		{
			return field_list<T0...>::append<T1...>();
		}

		template <typename... T0, template<typename T1, T1 x1, typename Tag1, typename... Attribs1> typename F, typename T1, T1 x1, typename Tag1, typename... Attribs1>
		auto operator,(const detail::field_list<T0...>&, const F<T1, x1, Tag1, utility::type_list<Attribs1...>>&)
		{
			return detail::field_list<T0...>::append<Tag1, F<T1, x1, Tag1, utility::type_list<Attribs1...>>>();
		}
	}

	template <typename T, typename F>
	void for_each_field(F f)
	{
		::muse::detail::for_each_field(T::fields(), f);
	}
}