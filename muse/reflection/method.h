#pragma once

#include "tag.h"
#include "fieldlist.h"

namespace muse
{
	namespace detail
	{
		template <typename Type, Type x, typename Tag, typename Attributes = utility::type_list<>>
		struct method;

		template <typename Type, typename Class, typename... Args, Type(Class::*Func)(Args...), typename Tag, typename... Attributes>
		struct method<Type(Class::*)(Args...), Func, Tag, utility::type_list<Attributes...>> : Attributes...
		{
			using type = Type(Class::*)(Args...);
			using tag_type = Tag;
			using class_type = Class;
			using return_type = Type;
			using args_type = utility::type_list<Args...>;
			static const char* name()                                                           { return Tag::c_str(); }
			static const auto call(Class& inst, Args... args)                                   { return (inst.*Func)(args...); }
			template <typename Attribute> auto operator()(const Attribute& attrib)              { return method<Type(Class::*)(Args...), Func, Tag, utility::type_list<Attribute>>(); }
			template <typename... Ts> auto operator()(const detail::field_list<Ts...>& attribs) { return method<Type(Class::*)(Args...), Func, Tag, utility::type_list<Ts...>>(); }
		};

		template <typename Type, typename Class, typename... Args, Type(Class::*Func)(Args...)const, typename Tag, typename... Attributes>
		struct method<Type(Class::*)(Args...)const, Func, Tag, utility::type_list<Attributes...>> : Attributes...
		{
			using type = Type(Class::*)(Args...)const;
			using tag_type = Tag;
			using class_type = Class;
			using return_type = Type;
			using args_type = utility::type_list<Args...>;
			static const char* name()                                                           { return Tag::c_str(); }
			static const auto call(Class& inst, Args... args)                                   { return (inst.*Func)(args...); }
			template <typename Attribute> auto operator()(const Attribute& attrib)              { return method<Type(Class::*)(Args...)const, Func, Tag, utility::type_list<Attribute>>(); }
			template <typename... Ts> auto operator()(const detail::field_list<Ts...>& attribs) { return method<Type(Class::*)(Args...)const, Func, Tag, utility::type_list<Ts...>>(); }
		};

		template <typename T0, T0 x0, typename Tag0, typename... Attribs0, template<typename T1, T1 x1, typename Tag1, typename... Attribs1> typename F, typename T1, T1 x1, typename Tag1, typename... Attribs1>
		auto operator,(const method<T0, x0, Tag0, utility::type_list<Attribs0...>>&, const F<T1, x1, Tag1, utility::type_list<Attribs1...>>&)
		{
			using Type0 = method<T0, x0, Tag0, utility::type_list<Attribs0...>>;
			using Type1 = F<T1, x1, Tag1, utility::type_list<Attribs1...>>;
			return detail::field_list<Tag0, Type0, Tag1, Type1>();
		}

		template<typename>
		struct is_method : std::false_type {};

		template<typename Type, Type x, typename Tag, typename Attributes>
		struct is_method<method<Type, x, Tag, Attributes>> : std::true_type {};
	}
}