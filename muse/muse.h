#pragma once

namespace muse
{
	template <typename, typename, typename Enable = void>
	struct meta_binding;
}

#include "util.h"
#include "reflection/attribute.h"
#include "reflection/method.h"
#include "reflection/property.h"
#include "reflection/tag.h"
#include "reflection/type.h"
#include "runtime/type.h"
#include "runtime/typeregistry.h"
#include "runtime/object.h"
#include "serialization/serializer.h"

#include "keywords.h"

#define MUSE_CONCAT2(X, Y) X##Y
#define MUSE_CONCAT(X, Y) MUSE_CONCAT2(X, Y)

#define MUSE_STRINGIFY2(X) #X
#define MUSE_STRINGIFY(X) MUSE_STRINGIFY2(X)

namespace muse
{
	template <typename A, typename T>
	using has_attribute = std::is_base_of<A, T>;

	template <bool Has>
	using check_attribute = typename std::enable_if<Has, int>::type;

	template <typename T, check_attribute<has_attribute<alias_attribute, T>::value> = 1>
	const char* get_alias(T& inst)
	{
		return T::alias_name();
	}

	template <typename T, check_attribute<!has_attribute<alias_attribute, T>::value> = 0>
	const char* get_alias(T& inst)
	{
		return "";
	}

	namespace detail
	{
		template <typename Default, typename AlwaysVoid, template<typename...> typename Op, typename... Args>
		struct detector
		{
			using value_type = std::false_type;
			using type = Default;
		};

		template <typename Default, template<typename...> typename Op, typename... Args>
		struct detector<Default, std::void_t<Op<Args...>>, Op, Args...>
		{
			using value_type = std::true_type;
			using type = Op<Args...>;
		};
	} // namespace detail

	struct none_such
	{
		none_such() = delete;
		~none_such() = delete;
		none_such(none_such const&) = delete;
		void operator=(none_such const&) = delete;
	};

	template <template<typename...> typename Op, typename... Args>
	using is_detected = typename detail::detector<none_such, void, Op, Args...>::Value;

	template <template<typename...> typename Op, typename... Args>
	using detected_t = typename detail::detector<none_such, void, Op, Args...>::Type;

	template <typename Default, template<typename...> typename Op, typename... Args>
	using detected_or = detail::detector<Default, void, Op, Args...>;

	template< template<typename...> typename Op, typename... Args >
	constexpr bool is_detectedV = is_detected<Op, Args...>::Value;

	template< typename Default, template<typename...> typename Op, typename... Args >
	using detected_orT = typename detected_or<Default, Op, Args...>::Type;

	template <typename Expected, template<typename...> typename Op, typename... Args>
	using is_detected_exact = std::is_same<Expected, detected_t<Op, Args...>>;

	template <typename Expected, template<typename...> typename Op, typename... Args>
	constexpr bool is_detected_exactV = is_detected_exact<Expected, Op, Args...>::value;

	template <typename To, template<typename...> typename Op, typename... Args>
	using is_detected_convertible = std::is_convertible<detected_t<Op, Args...>, To>;

	template <typename To, template<typename...> typename Op, typename... Args>
	constexpr bool is_detected_convertibleV = is_detected_convertible<To, Op, Args...>::value;
}