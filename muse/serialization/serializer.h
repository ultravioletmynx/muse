#pragma once

#include "../reflection/property.h"
#include "archive.h"

namespace muse
{
	struct serializer
	{
		template <typename Type, typename Class>
		static bool serialize_out(Class& object, const char* path) { return true; }

		template <typename Type, typename Class>
		static bool serialize_in(Class& object, const char* path) { return true; }

		template <typename Type, typename Class>
		static bool serialize(Class& object, const char* path, io::openmode mode = io::in)
		{
			if (mode == io::in)
				return serialize_in<Type>(object, path);
			else
				return serialize_out<Type>(object, path);
		}
	};
}