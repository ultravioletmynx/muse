#pragma once

#include "../reflection/property.h"
#include "../reflection/method.h"
#include "../reflection/type.h"

#include <any>
#include <unordered_map>

namespace muse
{
	namespace io
	{
		using openmode = uint8_t;
		static constexpr openmode in = 0;
		static constexpr openmode out = 1;
	}

	template <io::openmode mode>
	struct archive
	{
		explicit archive() {}

		static inline const bool is_input() { return mode == io::in; }
		static inline const bool is_output() { return mode == io::out; }

		void clear() { data.clear(); }

		template<typename T>
		bool operator()(const T& value, const char* name = "") {};

		std::unordered_map<std::string, std::any> data;
	};
	
	template <typename Class, typename Property>
	typename std::enable_if<detail::is_property<Property>::value, std::ostream>::type& operator<<(std::ostream& ar, const util::meta_binding<Class, Property>& field)
	{
		const char* name = field.field.name();
		std::size_t size = field.field.size();
		auto value = field.field.get_value(field.inst);
		std::size_t name_len = strlen(name) + 1;
		ar << name_len << name << size << value;
		return ar;
	}

	template <typename Class, typename Property>
	typename std::enable_if<detail::is_property<Property>::value, std::istream>::type& operator>>(std::istream& ar, util::meta_binding<Class, Property>& field)
	{
		std::size_t name_len = 0;
		ar >> name_len;

		std::string name(name_len, ' ');
		ar >> name;

		if (strcmp(name.c_str(), field.field.name()) == 0)
		{
			std::size_t size = 0;
			ar >> size;
			if (size == field.field.size())
			{
				std::remove_pointer<Property::value_type>::type value;
				ar >> value;
				field.field.set_value(field.inst, value);
			}
		}
		return ar;
	}

	template <typename Class, typename Method>
	typename std::enable_if<detail::is_method<Method>::value, std::istream>::type& operator<<(std::istream& ar, const util::meta_binding<Class, Method>& field) { return ar; }

	template <typename Class, typename Method >
	typename std::enable_if<detail::is_method<Method>::value, std::ostream>::type& operator>>(std::ostream& ar, util::meta_binding<Class, Method>& field) { return ar; }

	/*template <typename Class, typename Type>
	typename std::enable_if<detail::is_type<Type>::value, std::ostream>::type& operator<<(std::ostream& ar, const util::meta_binding<Class, Type>& field)
	{
		const char* name = typeid(Class).name();
		std::size_t name_len = strlen(name) + 1;
		ar << name_len << name;
		return ar;
	}*/
}