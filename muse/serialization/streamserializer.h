#pragma once

#include "../reflection/property.h"

#include "serializer.h"

#include <fstream>

namespace muse
{
	struct stream_serializer : serializer
	{
		template <typename Type, typename Class>
		static bool serialize_in(Class& object, const char* path)
		{
			std::ifstream stream(path, std::ifstream::binary);
			if (!stream.is_open() || stream.bad() || stream.fail() || stream.eof())
				return false;

			::muse::for_each_field<Type>([&](auto field)
			{
				util::meta_binding<Class, Type> bind(object, field);
				stream >> bind;
			});

			stream.close();

			return true;
		}

		template <typename Type, typename Class>
		static bool serialize_out(Class& object, const char* path)
		{
			std::ofstream stream(path, std::ofstream::binary);
			if (!stream.is_open() || stream.bad() || stream.fail())
				return false;

			::muse::for_each_field<Type>([&](auto field)
			{
				util::meta_binding<Class, Type> bind(object, field);
				stream << bind;
			});

			stream.close();

			return true;
		}
	};
}