#pragma once

#include <memory>
#include <typeindex>
#include <unordered_map>

#include "method.h"
#include "property.h"

namespace muse
{
	namespace runtime
	{
		class property;
		class method;

		struct type
		{
			using method_map_type = std::unordered_map<std::string, std::unique_ptr<runtime::method>>;
			using property_map_type = std::unordered_map<std::string, std::unique_ptr<runtime::property>>;

			virtual ~type() {}
			virtual method_map_type& methods() const = 0;
			virtual property_map_type& properties() const = 0;
			virtual method* get_method(const std::string& name) = 0;
			virtual property* get_property(const std::string& name) = 0;

			virtual bool serialize_object(const std::any& inst, const char* path, io::openmode mode = io::in) = 0;
		};

		namespace detail
		{
			template <typename Class, typename T>
			class type : public muse::runtime::type
			{
			public:
				virtual method_map_type& methods() const override
				{
					return method_map();
				}

				virtual property_map_type& properties() const override
				{
					return property_map();
				}

				virtual runtime::method* get_method(const std::string& name) override
				{
					auto& methods = method_map();
					const auto& it = methods.find(name);
					if (it != end(methods))
					{
						return it->second.get();
					}
					return nullptr;
				}

				virtual runtime::property* get_property(const std::string& name) override
				{
					auto& properties = property_map();
					const auto& it = properties.find(name);
					if (it != end(properties))
					{
						return it->second.get();
					}
					return nullptr;
				}

				virtual bool serialize_object(const std::any& inst, const char* path, io::openmode mode = io::in) override
				{
					Class& object = *std::any_cast<Class*>(inst);
					return T::serialize_object(object, path, mode);
				}

				template <typename Type, Type x, typename Tag, typename Attributes>
				static void register_field(const ::muse::detail::method<Type, x, Tag, Attributes>&)
				{
					auto& methods = method_map();
					methods.emplace(::muse::detail::method<Type, x, Tag, Attributes>::name(), std::make_unique<method<::muse::detail::method<Type, x, Tag, Attributes>>>());
				}

				template <typename Type, Type x, typename Tag, typename Attributes>
				static void register_field(const ::muse::detail::property<Type, x, Tag, Attributes>& prop)
				{
					auto& properties = property_map();
					properties.emplace(::muse::detail::property<Type, x, Tag, Attributes>::name(), std::make_unique<property<::muse::detail::property<Type, x, Tag, Attributes>>>());
				}

			private:
				static auto& method_map()
				{
					static method_map_type methodMap;
					return methodMap;
				}
			
				static auto& property_map()
				{
					static property_map_type propertyMap;
					return propertyMap;
				}
			};
		}
	}
}