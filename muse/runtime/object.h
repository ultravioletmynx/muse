#pragma once

namespace muse
{
	class object
	{
	public:
		virtual runtime::type* runtime_type() = 0;
	};

	namespace runtime
	{
		struct type;

		template <typename Class>
		class object : ::muse::object
		{
		public:
			virtual type* runtime_type() override
			{
				return type_registry::find_type<Class>();
			}
		};
	}

	static bool serialize_runtime_object(object& obj, const char* path, io::openmode mode = io::in)
	{
		runtime::type* runtime_type = obj.runtime_type();
		return runtime_type->serialize_object(&obj, path, mode);
	}
}