#pragma once

#include "../util.h"
#include <any>

namespace muse
{
	namespace runtime
	{
		namespace detail
		{
			class method_caller;
		}

		class method
		{
		public:
			virtual ~method() {}
			virtual const char* name() const = 0;

			template<typename Class, typename... Args>
			std::any call(Class& inst, Args... args) const
			{
				detail::method_caller_impl<utility::type_list<Args...>> caller(std::forward<Args>(args)...);
				return call_method(&inst, caller);
			}

		protected:
			virtual std::any call_method(const std::any& inst, detail::method_caller& caller) const = 0;
		};

		namespace detail
		{
			class method_caller
			{
			public:
				virtual ~method_caller() {}
				virtual std::string arg_str() const = 0;
			};

			template<typename Args>
			class method_caller_impl;

			template<typename... Args>
			class method_caller_impl<utility::type_list<Args...>> final : public method_caller
			{
			public:
				method_caller_impl(Args&&... args)
					: m_args(std::forward<Args>(args)...)
				{
				}

				template<typename T>
				typename std::enable_if<!std::is_void<typename T::return_type>::value, std::any>::type call(const std::any& inst) const
				{
					return call_seq<T>(inst, std::index_sequence_for<Args...>{});
				}

				template<typename T>
				typename std::enable_if<std::is_void<typename T::return_type>::value, std::any>::type call(const std::any& inst) const
				{
					call_seq<T>(inst, std::index_sequence_for<Args...>{});
					return std::any();
				}

				virtual std::string arg_str() const
				{
					std::stringstream stream;
					stream << utility::type_list<Args...>();
					return stream.str();
				}

			private:
				template<typename T, std::size_t... I>
				typename T::return_type call_seq(const std::any& inst, std::index_sequence<I...>) const
				{
					auto object = std::any_cast<T::class_type*>(inst);
					return T::call(*object, std::get<I>(m_args)...);
				}

				std::tuple<Args...> m_args;
			};

			template<typename T>
			class method final : public muse::runtime::method
			{
			public:
				virtual const char* name() const override
				{
					return T::name();
				}

			protected:
				virtual std::any call_method(const std::any& inst, method_caller& caller) const override
				{
					try
					{
						auto caller_impl = dynamic_cast<method_caller_impl<T::args_type>*>(&caller);
						if (!caller_impl)
						{
							std::ostringstream stream;
							stream << "Attempted to call method '" << T::name() << "' with invalid arguments.\nArguments '" << caller.arg_str() << "' provided, expected '" << T::args_type() << "'";
							throw std::runtime_error(stream.str());
						}
						return caller_impl->call<T>(inst);
					}
					catch (const std::runtime_error&)
					{
						//std::cout << err.what() << std::endl; // TODO: error handling
						return std::any();
					}
				}
			};
		}
	}
}