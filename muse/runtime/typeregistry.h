#pragma once

#include "type.h"

namespace muse
{
	namespace runtime
	{
		struct type_registry
		{
			using TypeMap = std::unordered_map<std::type_index, std::unique_ptr<type>>;
			static TypeMap& type_map()
			{
				static TypeMap types;
				return types;
			}

			template <typename T, typename F>
			static void register_runtime_type()
			{
				auto& types = type_map();
				detail::type<T, F>* runtime_type = new detail::type<T, F>();
				types.emplace(typeid(T), std::unique_ptr<detail::type<T, F>>(runtime_type));

				::muse::for_each_field<F>([&runtime_type](auto field)
				{
					runtime_type->register_field(field);
				});
			}

			template <typename T>
			static type* find_type()
			{
				return find_type(typeid(T));
			}

			static type* find_type(const std::type_index& index)
			{
				const auto& types = type_map();
				const auto it = types.find(index);
				if (it != end(types))
				{
					return it->second.get();
				}
				return nullptr;
			}
		};
	}
}