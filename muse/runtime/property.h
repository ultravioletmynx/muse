#pragma once

#include "../serialization/archive.h"

namespace muse
{
	namespace runtime
	{
		class property
		{
		public:
			virtual ~property() {}
			virtual const char* name() const = 0;
			virtual const std::size_t size() const = 0;

			template <typename Class>
			std::any get_value(const Class& inst)
			{
				return get_value_internal(&inst);
			}
			
			template <typename Class, typename Type>
			void set_value(Class& inst, const Type& value)
			{
				set_value_internal(&inst, value);
			}

			template <typename Class, typename Type>
			bool load(Class& inst, std::istream& stream)
			{
				return load_internal(&inst, stream);
			}

			template <typename Class, typename Type>
			bool save(Class& inst, std::ostream& stream)
			{
				return save_internal(&inst, stream);
			}

		private:
			virtual std::any get_value_internal(const std::any& inst) const = 0;
			virtual void set_value_internal(const std::any& inst, const std::any& value) const = 0;
			virtual bool load_internal(const std::any& inst, std::istream& stream) const = 0;
			virtual bool save_internal(const std::any& inst, std::ostream& stream) const = 0;
		};

		namespace detail
		{
			template <typename T>
			struct property : muse::runtime::property
			{
				virtual const char* name() const override
				{
					return T::name();
				}

				virtual const std::size_t size() const override
				{
					return sizeof(T);
				}

				virtual std::any get_value_internal(const std::any& inst) const override
				{
					auto object = std::any_cast<const T::class_type*>(inst);
					return T::get_value(*object);
				}

				virtual void set_value_internal(const std::any& inst, const std::any& value) const override
				{
					auto object = std::any_cast<T::class_type*>(inst);
					auto val = std::any_cast<const T::value_type&>(value);
					T::set_value(*object, val);
				}

				virtual bool load_internal(const std::any& inst, std::istream& stream) const override
				{
					T::class_type& object = *std::any_cast<T::class_type*>(inst);
					T field = T();
					util::meta_binding<T::class_type, T> bind(object, field);
					stream >> bind;
					return true;
				}

				virtual bool save_internal(const std::any& inst, std::ostream& stream) const override
				{
					T::class_type& object = *std::any_cast<T::class_type*>(inst);
					T field = T();
					util::meta_binding<T::class_type, T> bind(object, field);
					stream << bind;
					return true;
				}
			};
		}
	}
}