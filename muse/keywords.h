#pragma once

namespace muse
{
#define metatype(NAME) MUSE_CONCAT(___dummy_type_namespace_, NAME) = void; namespace MUSE_CONCAT(muse_type_desc_, NAME) { using namespace ::muse; using reflected_class = NAME ; } namespace MUSE_CONCAT(muse_type_desc_, NAME)
#define metaobject using meta_object
#define metaclass type<reflected_class>

#ifdef __INTELLISENSE__
	// Workaround for intellisense or other IDE syntax highlighters/inspectors
	// The actual implementations are heavy in template meta-programming and don't play nice with IDE syntax highlighting or deduction (intellisense dislikes decltype in particular)
	// These essentially define a makeshift "user interface" that directly mirrors the real implementations
#define typeof(NAME) ::muse::metatype
#define tag(STR) char
#define member(STR) tag(MUSE_STRINGIFY(STR))

	struct property
	{
		using type = void;
		using tag_type = void;
		using class_type = void;

		property() {}

		template<typename T>
		property(T) {}

		template <typename T>
		auto operator()(const T& t) { return t; }
	};

	struct method
	{
		using type = void;
		using tag_type = void;
		using class_type = void;
		using return_type = void;
		using args_type = void;

		method() {}

		template<typename T>
		method(T) {}

		template <typename T>
		auto operator()(const T& t) { return t; }
	};

	struct metatype
	{
		template <typename T >
		static bool serialize_object(T& object, const char* path, io::openmode mode = io::in);

		struct properties
		{
			struct property
			{
				template <typename T>
				static std::any get_value(T&);

				template <typename T>
				static void set_value(T&, std::any);
			};

			template <typename Tag>
			using get = property;
		};

		struct methods
		{
			struct method
			{
				template <typename... Ts>
				static std::any call(Ts...);
			};

			template <typename Tag>
			using get = method;
		};
	};

	template <typename T, typename Tag, typename = typename std::enable_if<std::is_same<metatype, T>::value>::type>
	using get_property = metatype::properties::property;

	template <typename T, typename Tag, typename = typename std::enable_if<std::is_same<metatype, T>::value>::type>
	using get_method = metatype::methods::method;

	struct alias
	{
		alias(const char*) {}
	};
#else
#define tag(STR) ::muse::detail::tag<MACRO_GET_32(STR, 0), 0>
#define member(STR) tag(MUSE_STRINGIFY(STR))
#define property(NAME) ::muse::detail::property<typename ::muse::util::member_pointer_traits<decltype(MUSE_CONCAT(&reflected_class::, NAME))>::type, MUSE_CONCAT(&reflected_class::, NAME), member(NAME)>()
#define method(NAME) ::muse::detail::method<typename ::muse::util::member_pointer_traits<decltype(MUSE_CONCAT(&reflected_class::, NAME))>::type, MUSE_CONCAT(&reflected_class::, NAME), member(NAME)>()
#define alias(TAG) ::muse::detail::alias_attribute<tag(TAG)>()
#define typeof(NAME) std::conditional<std::is_class<MUSE_CONCAT(::muse_type_desc_, NAME)::meta_object>::value, MUSE_CONCAT(::muse_type_desc_, NAME)::meta_object, void>::type

	template <typename T, typename Tag, typename = typename std::enable_if<detail::is_type<T>::value>::type>
	using get_property = typename T::properties::template get<Tag>::type;

	template <typename T, typename Tag, typename = typename std::enable_if<detail::is_type<T>::value>::type>
	using get_method = typename T::methods::template get<Tag>::type;
#endif
}