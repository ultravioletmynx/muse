#include "test.h"

#include <iostream>

// compile time meta type for class test
using meta = typeof(test);

// compile time meta fields
using meta_test_member = muse::get_property<meta, member(test_member)>;
using meta_get_member = muse::get_method<meta, member(get_member)>;
using meta_set_member = muse::get_method<meta, member(set_member)>;
using meta_print_something = muse::get_method<meta, member(print_something)>;

namespace
{
	template <typename Last>
	inline void print(Last&& last)
	{
		std::cout << std::forward<Last>(last) << std::endl;
	}

	template<typename First, typename... Rest>
	inline void print(First&& first, Rest&&... rest)
	{
		std::cout << std::forward<First>(first);
		print(std::forward<Rest>(rest)...);
	}

	bool test_serialization(test& x)
	{
		print("//==============================\nTesting serialization...\n");

		x.test_member = 124;
		if (!meta::serialize_object(x, "test.bin", muse::io::out))
		{
			print("Failed to save object...\n");
			return false;
		}

		print("Saved Test Member: ", x.test_member);

		x.test_member = 4;
		print("Internal Test Member: ", x.test_member);

		if (!meta::serialize_object(x, "test.bin", muse::io::in))
		{
			print("Failed to load object...\n");
			return false;
		}

		print("Loaded Test Member: ", x.test_member);

		return true;
	}

	bool test_compile_time(test& x)
	{
		print("//==============================\nTesting compile-time reflection...\n");

		meta_print_something::call(x, 1, const_cast<int*>(&x.test_member), 3.0, "Helloooo");

		print(meta_test_member::get_value(x));
		meta_test_member::set_value(x, 16);
		print(meta_test_member::get_value(x));
		print(meta_get_member::call(x));
		meta_set_member::call(x, 32);
		print(meta_get_member::call(x));

		meta_print_something::call(x, 1, const_cast<int*>(&x.test_member), 3.0, "Helloooo");

		muse::for_each_field<meta>([](auto field)
		{
			print(field.name());
		});

		return true;
	}

	bool test_runtime(test& x)
	{
		print("//==============================\nTesting run-time reflection...\n");

		auto type = x.runtime_type();
		auto method = type->get_method("print_something");
		method->call(x, 1, const_cast<int*>(&x.test_member), 3.0, "Helloooo");

		method = type->get_method("get_a_value");
		auto ret = method->call(x);
		print(std::any_cast<int>(ret));

		auto prop = type->get_property("test_member");
		auto value = prop->get_value(x);
		print(std::any_cast<int>(value));
		prop->set_value(x, 2);

		value = prop->get_value(x);
		print(std::any_cast<int>(value));

		print(muse::get_alias(meta()));
		print(muse::get_alias(typeof(test_base)()));
		print(muse::get_alias(meta_test_member()));

		auto& props = type->properties();
		for (auto& prop : props)
		{
			print(prop.first.c_str());
		}

		auto& methods = type->methods();
		for (auto& method : methods)
		{
			print(method.first.c_str());
		}

		return true;
	}
}

bool test::run()
{
	bool result = true;
	result &= test_serialization(*this);
	result &= test_compile_time(*this);
	result &= test_runtime(*this);
	return result;
}

void test::print_something(int x, int* y, double z, const char* w)
{
	print(x, "\n", *y, "\n", z, "\n", w, "\n");
}