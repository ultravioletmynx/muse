#pragma once

#include <windows.h>

#include "muse/muse.h"

class test_base
{
public:
	test_base() {}

	int test_member = 32;
};

class test : public muse::runtime::object<test>
{
public:
	test() {};
	~test() {};

	bool run();

	int test_member = 4;
	const int test_const_member = 8;

	int* test_ptr_member = &test_member;
	const int* test_const_ptr_member = &test_const_member;

	int get_member() { return test_member; }
	void set_member(const int& value) { test_member = value; }

	void print_something(int x, int* y, double z, const char* w);

	int get_a_value()
	{
		return 243;
	}
};

using metatype(test_base)
{
	metaobject = decltype
	(
		metaclass()
		(
			alias("Test Base")
		)
		[
			property(test_member)
			(
				alias("Test Member Alias")
			)
		]
	);
}

using metatype(test) 
{
	metaobject = decltype
	(
		metaclass()
		(
			alias("Test")
		)
		[
			property(test_member)
			(
				alias("Test Member Alias")
			),
			property(test_ptr_member),
			property(test_const_member),
			property(test_const_ptr_member),
			method(get_member)
			(
				alias("Get Member Alias")
			),
			method(set_member),
			method(print_something),
			method(get_a_value)
		]
	);
}